#include "ray.h"
#include "material.h"
#include "light.h"

#include "../fileio/bitmap.h"

using namespace std;
extern bool debugMode;

// Apply the phong model to this point on the surface of the object, returning
// the color of that point.
Vec3d Material::shade( Scene *scene, const ray& r, const isect& i ) const
{
	// YOUR CODE HERE

	// For now, this method just returns the diffuse color of the object.
	// This gives a single matte color for every distinct surface in the
	// scene, and that's it.  Simple, but enough to get you started.
	// (It's also inconsistent with the phong model...)

	// Your mission is to fill in this method with the rest of the phong
	// shading model, including the contributions of all the light sources.
    // You will need to call both distanceAttenuation() and shadowAttenuation()
    // somewhere in your code in order to compute shadows and light falloff.
	if( debugMode )
		std::cout << "Debugging Phong code..." << std::endl;

	// When you're iterating through the lights,
	// you'll want to use code that looks something
	// like this:
	//
	// for ( vector<Light*>::const_iterator litr = scene->beginLights(); 
	// 		litr != scene->endLights(); 
	// 		++litr )
	// {
	// 		Light* pLight = *litr;
	// 		.
	// 		.
	// 		.
	// }

	Vec3d i_point = r.getPosition() + (r.getDirection() * i.t);    // point of intersection
	
	Vec3d value = prod(ka(i), scene->ambient());	// cumulative color value starts with ambient lighting
	
	for ( vector<Light*>::const_iterator litr = scene->beginLights(); litr != scene->endLights(); ++litr ) {
	    Light* pLight = *litr;
		double light_dist = pLight->getDistance(i_point);

	    Vec3d light_color = pLight->getColor(i_point) * pLight->distanceAttenuation(i_point);	// brightness of light

	    ray shadowRay(i_point, pLight->getDirection(i_point), ray::SHADOW);

        int count = 0;
		double total_dist = 0.0;
        isect shadow_occluder;
        while (count < 10 && scene->intersect(shadowRay, shadow_occluder)) {
			total_dist += shadow_occluder.t;
			if (total_dist > light_dist) {
	            // ray goes past the light, so these objects do not contribute to the shadow
				break;
			}
			Vec3d occluder_color = shadow_occluder.getMaterial().kt(shadow_occluder);
			if (occluder_color == ZeroVec3d) {
				// completely obscured, so no need to continue iterating toward light
				light_color = ZeroVec3d;
				break;
			}
            light_color = prod(light_color, occluder_color);
			Vec3d next_point = shadowRay.getPosition() + (shadowRay.getDirection() * shadow_occluder.t);
            ray newRay(next_point, shadowRay.getDirection(), ray::SHADOW);
            if (isnan(newRay.getPosition()[0])) {
                if (debugMode) {
                    cout << "shadow ray not a number" << endl;
                }
                break;
            }
	      	shadowRay = newRay;
            count++;
        }
        /*
        if (debugMode) {
            cout << "shadowRay ended at " << (shadowRay.getPosition() + (shadowRay.getDirection() * shadow_occluder.t)) << endl;
            cout << "count was " << count << endl;
        }
	    */

	    Vec3d half = pLight->getDirection(i_point) - r.getDirection();
	    half.normalize();
	    
	    double dot_product = clamp(i.N * pLight->getDirection(i_point));
	    value += dot_product * prod(kd(i), light_color);	// diffuse lighting

	    value += pow(clamp(half * i.N), shininess(i)) * prod(ks(i), light_color);	// specular lighting
	}
	
	value.clamp();
	return value;
}


TextureMap::TextureMap( string filename )
{
    data = readBMP( filename.c_str(), width, height );
    if( 0 == data )
    {
        width = 0;
        height = 0;
        string error( "Unable to load texture map '" );
        error.append( filename );
        error.append( "'." );
        throw TextureMapException( error );
    }
}

Vec3d TextureMap::getMappedValue( const Vec2d& coord ) const
{
	// YOUR CODE HERE

    // In order to add texture mapping support to the 
    // raytracer, you need to implement this function.
    // What this function should do is convert from
    // parametric space which is the unit square
    // [0, 1] x [0, 1] in 2-space to bitmap coordinates,
    // and use these to perform bilinear interpolation
    // of the values.



    return Vec3d(1.0, 1.0, 1.0);

}


Vec3d TextureMap::getPixelAt( int x, int y ) const
{
    // This keeps it from crashing if it can't load
    // the texture, but the person tries to render anyway.
    if (0 == data)
      return Vec3d(1.0, 1.0, 1.0);

    if( x >= width )
       x = width - 1;
    if( y >= height )
       y = height - 1;

    // Find the position in the big data array...
    int pos = (y * width + x) * 3;
    return Vec3d( double(data[pos]) / 255.0, 
       double(data[pos+1]) / 255.0,
       double(data[pos+2]) / 255.0 );
}

Vec3d MaterialParameter::value( const isect& is ) const
{
    if( 0 != _textureMap )
        return _textureMap->getMappedValue( is.uvCoordinates );
    else
        return _value;
}

double MaterialParameter::intensityValue( const isect& is ) const
{
    if( 0 != _textureMap )
    {
        Vec3d value( _textureMap->getMappedValue( is.uvCoordinates ) );
        return (0.299 * value[0]) + (0.587 * value[1]) + (0.114 * value[2]);
    }
    else
        return (0.299 * _value[0]) + (0.587 * _value[1]) + (0.114 * _value[2]);
}

