#include <cmath>

#include "light.h"
#include <limits>


using namespace std;

double DirectionalLight::getDistance( const Vec3d& P) const
{
	return std::numeric_limits<double>::max();
}

double DirectionalLight::distanceAttenuation( const Vec3d& P ) const
{
	// distance to light is infinite, so f(di) goes to 0.  Return 1.
	return 1.0;
}

Vec3d DirectionalLight::shadowAttenuation( const Vec3d& P ) const
{
    // YOUR CODE HERE:
    // You should implement shadow-handling code here.

    return Vec3d(1,1,1);

}

Vec3d DirectionalLight::getColor( const Vec3d& P ) const
{
	// Color doesn't depend on P 
	return color;
}

Vec3d DirectionalLight::getDirection( const Vec3d& P ) const
{
	return -orientation;
}

/* * * distance to a point light // davilla * * */
double PointLight::getDistance( const Vec3d& P ) const
{
	Vec3d diff = P - position;
	return diff.length();
}

double PointLight::distanceAttenuation( const Vec3d& P ) const
{

	// YOUR CODE HERE

	// You'll need to modify this method to attenuate the intensity 
	// of the light based on the distance between the source and the 
	// point P.  For now, we assume no attenuation and just return 1.0
	double l = getDistance(P);
	return 1 / (constantTerm + (linearTerm + quadraticTerm * l) * l);
}

Vec3d PointLight::getColor( const Vec3d& P ) const
{
	// Color doesn't depend on P 
	return color;
}

Vec3d PointLight::getDirection( const Vec3d& P ) const
{
	Vec3d ret = position - P;
	ret.normalize();
	return ret;
}

Vec3d PointLight::shadowAttenuation(const Vec3d& P) const
{
    // YOUR CODE HERE:
    // You should implement shadow-handling code here.

    return Vec3d(1,1,1);

}
