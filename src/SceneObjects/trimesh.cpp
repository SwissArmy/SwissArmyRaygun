#include <cmath>
#include <float.h>
#include "trimesh.h"

using namespace std;

extern bool debugMode;

Trimesh::~Trimesh()
{
	for( Materials::iterator i = materials.begin(); i != materials.end(); ++i )
		delete *i;
}

// must add vertices, normals, and materials IN ORDER
void Trimesh::addVertex( const Vec3d &v )
{
    vertices.push_back( v );
}

void Trimesh::addMaterial( Material *m )
{
    materials.push_back( m );
}

void Trimesh::addNormal( const Vec3d &n )
{
    Vec3d norm_n = n;
    norm_n.normalize();
    normals.push_back( norm_n );
}

// Returns false if the vertices a,b,c don't all exist
bool Trimesh::addFace( int a, int b, int c )
{
    int vcnt = vertices.size();

    if( a >= vcnt || b >= vcnt || c >= vcnt )
        return false;

    TrimeshFace *newFace = new TrimeshFace( scene, new Material(*this->material), this, a, b, c );
    newFace->setTransform(this->transform);
    faces.push_back( newFace );
    scene->add(newFace);
    return true;
}

char *
Trimesh::doubleCheck()
// Check to make sure that if we have per-vertex materials or normals
// they are the right number.
{
    if( !materials.empty() && materials.size() != vertices.size() )
        return (char *)"Bad Trimesh: Wrong number of materials.";
    if( !normals.empty() && normals.size() != vertices.size() )
        return (char *)"Bad Trimesh: Wrong number of normals.";

    return 0;
}

// Intersect ray r with the triangle abc.  If it hits returns true,
// and put the parameter in t and the barycentric coordinates of the
// intersection in bary.
// Uses the algorithm and notation from _Graphic Gems 5_, p. 232.
//
// Calculates and returns the normal of the triangle too.
bool TrimeshFace::intersectLocal( const ray& r, isect& i ) const
{
	
	const Vec3d pos = r.getPosition();
	const Vec3d dir = r.getDirection();

    const Vec3d& v0 = parent->vertices[ids[0]];
    const Vec3d& v1 = parent->vertices[ids[1]];
    const Vec3d& v2 = parent->vertices[ids[2]];

    // I think this is wrong. -Josh
	/*
	if( dir[2] == 0.0 ) {
		return false;
	}
	const double t = -pos[2]/dir[2];
	*/

	// compute the intersection point of the ray with the plane containing the triangle
	const Vec3d Q1 = v1 - v0;
	const Vec3d Q2 = v2 - v0;
	Vec3d plane_normal = Q1 ^ Q2;	// cross product
	plane_normal.normalize();
	const double t = ((v0 - pos) * plane_normal) / (dir * plane_normal);

	if (t <= RAY_EPSILON) return false;
	const Vec3d P = r.at(t);

	if (debugMode) {
		cout << v0 << " " << v1 << " " << v2 << " : " << t << " " << P << endl;
	}
	
	// calculate the berycentric coordinates
	const Vec3d R = P - v0;
	const double a = Q1 * Q1;
	const double b = Q1 * Q2;
	const double d = Q2 * Q2;
	const double det = 1 / (a * d - b * b);
	if (isnan(det)) return false;
	const double v = det * (d * (R * Q1) - b * (R * Q2));
	if (v < 0 || v > 1) return false;
	const double w = det * (a * (R * Q2) - b * (R * Q1));
	if (w < 0 || w > 1) return false;
	const double u = 1 - v - w;	
	if (u < 0 || u > 1) return false;
	
	// all berycentric coordinates are within [0, 1]
	// ray intersects so set intersection point

	if (debugMode) {
		cout << "berycentric: [" << u << "] , [" << v << "] , [" << w << "]" << endl;
	}

	// set iSect with intersect dist, face normal and berycentric coords
	i.obj = this;
	i.t = t;	
	i.N = plane_normal;
    i.setUVCoordinates( Vec2d(u, v) );

	return true;
}

void
Trimesh::generateNormals()
// Once you've loaded all the verts and faces, we can generate per
// vertex normals by averaging the normals of the neighboring faces.
{
     normals.clear();

     int cnt = vertices.size();
     normals.resize( cnt );	// initializes entries to the zero vector

     for( Faces::iterator fi = faces.begin(); fi != faces.end(); ++fi )
     {
         Vec3d a = vertices[(**fi)[0]];
         Vec3d b = vertices[(**fi)[1]];
         Vec3d c = vertices[(**fi)[2]];

         Vec3d faceNormal = ((b-a) ^ (c-a));
         faceNormal.normalize();

         for( int i = 0; i < 3; ++i )
         {
             normals[(**fi)[i]] += faceNormal;
         }
     }

     for( int i = 0; i < cnt; ++i )
     {
         normals[i].normalize();
     }
}

